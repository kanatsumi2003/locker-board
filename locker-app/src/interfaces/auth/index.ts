export interface IAuthItem {
  accessToken: string;
  refreshToken: string;
}

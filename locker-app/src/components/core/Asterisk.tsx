function Asterisk() {
  return <span className="text-red-600 text-4xl font-bold">*</span>;
}

export default Asterisk;

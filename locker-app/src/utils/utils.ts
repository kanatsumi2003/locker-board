import { ILocation } from "@/interfaces";

export const range = (start: number, end: number) => {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
};

export function formatTime(duration: number) {
  // Hours, minutes and seconds
  const hrs = ~~(duration / 3600);
  const mins = ~~((duration % 3600) / 60);
  const secs = ~~duration % 60;

  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = "";

  if (hrs > 0) {
    ret += "" + hrs + " giờ ";
  }
  if (mins > 0) {
    ret += "" + mins + " phút ";
  }
  ret += "" + secs + " giây";

  return ret;
}

export function checkLocation(location: ILocation): boolean {
  return (
    !!location.district &&
    !!location.districtCode &&
    !!location.province &&
    !!location.provinceCode &&
    !!location.ward &&
    !!location.wardCode
  );
}

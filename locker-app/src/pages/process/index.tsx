import ProcessOrders from "@/components/process/ProcessOrders";

function ProcessPage() {
  return (
    <div className="p-10 h-full flex flex-col items-center gap-8">
      <ProcessOrders />
    </div>
  );
}

export default ProcessPage;

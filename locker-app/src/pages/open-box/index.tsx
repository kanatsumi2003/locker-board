import OpenBoxQr from "@/components/open-box/OpenBoxQr";

function OpenBoxPage() {
  return (
    <div className="p-10 h-full flex flex-col items-center gap-8">
      <OpenBoxQr />
    </div>
  );
}

export default OpenBoxPage;

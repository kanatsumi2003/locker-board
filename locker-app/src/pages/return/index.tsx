import ReturnOrders from "@/components/return/ReturnOrders";

function ReceivePage() {
  return (
    <div className="p-10 h-full flex flex-col items-center gap-8">
      <ReturnOrders />
    </div>
  );
}

export default ReceivePage;

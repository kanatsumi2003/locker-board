import BoxesList from "@/components/boxes/BoxesList";

function ProcessPage() {
  return (
    <div className="p-10 h-full flex flex-col items-center gap-8">
      <BoxesList />
    </div>
  );
}

export default ProcessPage;

TOPIC_CONNECT = "lockers/connect"
TOPIC_DISCONNECT = "lockers/disconnect"
TOPIC_RESET_BOXES = "lockers/reset-boxes"
TOPIC_ADD_BOX = "lockers/add-box"
TOPIC_REMOVE_BOX = "lockers/remove-box"

TOPIC_OPEN_BOX = "lockers/open-box"
TOPIC_UPDATE_INFO = "lockers/update-info"
